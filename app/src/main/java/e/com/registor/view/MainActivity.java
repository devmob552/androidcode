package e.com.registor.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import e.com.registor.R;
import e.com.registor.model.signup_model.Data;

public class MainActivity extends AppCompatActivity {
    Data data;
    TextView  nameTV,genderTV,countryTV,mobileTV,passwordTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data= (Data) getIntent().getSerializableExtra("data");
         init();

    }

    public void init(){
        nameTV=findViewById(R.id.nameTV);
        genderTV=findViewById(R.id.genderTV);
        countryTV=findViewById(R.id.countryTV);
        mobileTV=findViewById(R.id.mobileTV);
        passwordTV=findViewById(R.id.passwordTV);

        nameTV.setText(nameTV.getText().toString()+data.getName());
        genderTV.setText(genderTV.getText().toString()+data.getGender());
        countryTV.setText(countryTV.getText().toString()+data.getCountry());
        mobileTV.setText(mobileTV.getText().toString()+data.getMobile());
        
        passwordTV.setText(passwordTV.getText().toString()+data.getPassword());

    }


}
