package e.com.registor.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.Serializable;

import e.com.registor.R;
import e.com.registor.model.signup_model.Data;
import e.com.registor.presenter.signup_presenter.SignupPresenter;

/**
 * Created by Mobile on 7/5/2018.
 */

public class SignUpActivity extends AppCompatActivity {
    ImageView backIV;
    public EditText nameET,genderET,countryET,mobileET,passwordET,conPassET;
    Button signupBT;
    Data data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        init();
    }

    public void init(){
        nameET=findViewById(R.id.nameET);
        genderET=findViewById(R.id.genderET);
        countryET=findViewById(R.id.countryET);
        mobileET=findViewById(R.id.mobileET);
        passwordET=findViewById(R.id.passwordET);
        conPassET=findViewById(R.id.conPassET);
        signupBT=findViewById(R.id.signupBT);

        signupBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data=new Data();
                data.setName(nameET.getText().toString().trim());
                data.setGender(genderET.getText().toString().trim());
                data.setCountry(countryET.getText().toString().trim());
                data.setMobile(mobileET.getText().toString().trim());
                data.setPassword(passwordET.getText().toString().trim());
                data.setConfPassword(conPassET.getText().toString().trim());

                SignupPresenter presenter=new SignupPresenter(SignUpActivity.this,data);
                presenter.requestAPI();

            }
        });

    }


    public void startActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("data", (Serializable) data);
        startActivity(intent);
        finish();
    }




}