package e.com.registor.webconnection;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mobile on 10/8/2018.
 */

public class WebConnection {

    public static final String baseUrl = "";
    private static WebHandler holder;
    private static WebHandler retrofitInstance;


    public WebConnection() {

    }


    public static WebHandler getInstance() {

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder ();

            Retrofit retrofit = new Retrofit.Builder ()
                    .baseUrl (baseUrl)
                    .addConverterFactory (GsonConverterFactory.create ()).client (httpClient.build ())
                    .build ();
            holder = retrofit.create (WebHandler.class);

        return holder;
    }

}
