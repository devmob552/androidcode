package e.com.registor.presenter.signup_presenter;

import android.widget.Toast;

import e.com.registor.model.signup_model.Data;
import e.com.registor.view.SignUpActivity;

/**
 * Created by Mobile on 10/9/2018.
 */

public class SignupPresenter implements ISignupPresenter {
    SignUpActivity activity;
    Data data;

    public SignupPresenter(SignUpActivity activity,Data data){
        this.activity=activity;
        this.data=data;
    }


    @Override
    public void requestAPI() {
        if(checkData()){
   Toast.makeText(activity,"Welcome",Toast.LENGTH_SHORT).show();
          activity.startActivity();
        }
    }

    private boolean checkData(){
        if(data.getName().equals("")){
            onFaliure("Enter Name");
        }else if(data.getGender().equals("")){
            onFaliure("Enter Gender");
        }else if(data.getCountry().equals("")){
            onFaliure("Enter Country");
        } else if(data.getMobile().equals("")){
            onFaliure("Enter Mobile no.");
        }else if(data.getPassword().equals("")){
            onFaliure("Enter Password");
        }else if(data.getConfPassword().equals("")){
            onFaliure("Enter Confirm Password");
        }else if(!data.getPassword().equals(data.getConfPassword())){
            onFaliure("Password and confirm Password must be Same!");
            activity.conPassET.setText("");
            activity.passwordET.setText("");
        }else{
         return true;
        }
        return false;
    }


    @Override
    public void onFaliure(String msg) {
        Toast.makeText(activity,msg,Toast.LENGTH_SHORT).show();
    }
}
